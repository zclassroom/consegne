## Chatroom o gioco TCP

I socket TCP sono i protagonisti di questa consegna, non sono ammesse altre tecnologie di networking (udp, http e web socket)

**[A]** Si realizzi un'applicazione Flutter per una chatroom -- luogo virtuale in cui più soggetti si incontrano - server in Dart (o node.js per i più bravi) e client in Flutter. 

**[B]** Si realizzi un gioco che preveda almeno 2 giocatori che si scontrano usando un client Flutter (possibile l'uso di engine tipo Flame o simili). Il server in Dart controlla l'interazione fra i giocatori.


## Valutazione

Contribuiscono alla valutazione: la pulizia del codice, la documentazione e le scelte operate. Il progetto deve essere consegnato sulla cartella `chatroom_tcp` ovvero `gioco_tcp` nel progetto (repository) `TPSIT` (o come è stato chiamato), la documentazione va nel file `README.md`. Le valutazioni eccellenti (8, 9, 10) sono riservate a coloro che daranno un consistente apporto personale e creativo con attenzione all'usabilità dell'applicazione.  

## nota per Android

Android gestisce in modo articolato i permessi. Se sì lavora non in modalità debug potrebbe risultare necessario unamodifica al file `AndroidManifest.xml` in `android/app/src/main` aggiungendo i permessi
```
<uses-permission android:name="android.permission.INTERNET"/>
```
[qui](https://developer.android.com/guide/topics/permissions/overview) per i dettagli).

