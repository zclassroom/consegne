# Dijkstra

Si realizzi la classe `MyGraph`, classe per un grafo orientato di `6` nodi, in `Java` con un campo privato (matrice) che rappresenta la **matrice delle adiacenze**: pesi positivi e `0` per assenza di archi, non sono previsti cappi, quindi `0` sulla diagonale principale. La classe `PrioriotyQueue` è una *inner class* di `MyGraph`. In `MyGraph` esiste un metodo *factory* ovvero un metodo `static` che resistuisce un grafo random: da un nodo escono al più 3 archi. In `Main`, classe di prova, si testa l'algorimto di Dijkstra.

## Valutazione

Contribuiscono alla valutazione: la pulizia del codice, la documentazione e le scelte operate. Il progetto deve essere consegnato sulla cartella `myGraph` nel progetto (repository) `Informatica` (o similare), la documentazione va nel file `README.md`. Le valutazioni eccellenti (8, 9, 10) sono riservate a coloro che realizzeranno le consegne e danno un consistente apporto personale eventualmente aggiungento *feature*. Altro elemento di valutazione è la generazione *random* dell'albero, non alla portata di tutti!
