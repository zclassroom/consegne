# blog

Usando VueJS (ver.3) e lavorando con la sua `cli` (installandola come paccheto Node in modo gobale) si realizzi una `pwa` REST che dialoghi con un server personale - `json-server` **[home](https://www.npmjs.com/package/json-server)** - o con API *free* in rete.

## Valutazione

Contribuiscono alla valutazione: la pulizia del codice, la documentazione e le scelte operate. La `pwa` deve essere consegnato nella cartella apposita nel progetto (repository) `informatica` (o come è stato chiamato), la documentazione va nel file `README.md`. Le valutazioni eccellenti (8, 9, 10) sono riservate a coloro che:
- daranno un consistente apporto personale e creativo con attenzione all'usabilità dell'applicazione,
- presenteranno la propria *app* alla commissione in seduta pomeridiana,
- realizzeranno il REST, `GET`, `POST`, `PUT` e `DELETE`, in modo completo oppure solo `GET` ma di una "rilevabile complessità".

**NB** Tutti gli studenti devono installare l'app sul telefonino. La `pwa` abbisogna di un server sicuro - `ssh` - *provider* gratuiti si trovano, ecco una possibilità **[netlify](https://www.netlify.com/)** consigliata. Se vi fossero problemi di CORS esistono pure PROXY che aggirano la cosa (in caso chiedere ai docenti). 
