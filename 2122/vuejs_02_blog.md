# blog

Usando VueJS (ver.3) ed un framework web-css, ad esempio Bulma, si reaizzi un Blog (stand alone) partendo dagli esempi proposti con le seguenti caratteristiche.
- Esiste un solo dato `posts` che contiene tutti i post.
- Ogni post possiede: `title`, `body`, `category`, `comments` e `tags` (commenti e tag per i voti eccellenti).
- Un post appartiene ad una sola categoria e può avere più tag, ogni post ha i suoi commenti. 

## Valutazione

Contribuiscono alla valutazione: la pulizia del codice, la documentazione e le scelte operate. Il progetto deve essere consegnato sulla cartella `blog` progetto (repository) `informatica` (o come è stato chiamato), la documentazione va nel file `README.md`. Le valutazioni eccellenti (8, 9, 10) sono riservate a coloro che daranno un consistente apporto personale e creativo con attenzione all'usabilità dell'applicazione ed introdurranno i tag ed i commenti gestendoli in modo usabile. 
