## 4IB AS 2021-2022

Cipolato Simone [repo](https://gitlab.com/SimoneCipolato/informatica2122).  
Compagno Filippo [repo](https://github.com/filippocompagno/Inf2122).  
Hoxha Jon [repo](https://gitlab.com/Ahxoh/inf2022-hoxha).   
Pellizzon Leonardo [repo](https://gitlab.com/leonardopellizzon/informatica2122).  
Pettenò Matteo [repo](https://gitlab.com/MatteoPetteno/Inf2122).  
Polesel Alex [repo](https://gitlab.com/alexpolesel/informatica2122).  
Puppa Giulio [repo](https://gitlab.com/GiulioPuppa550/informatica2122).   
Ragazzi Filippo [repo](https://gitlab.com/filippo.ragazzi/informatica2122).  
Ravagnin Mattia [repo](https://gitlab.com/mattia.ravagnin/Informatica2122).  
Riato Matteo [repo](https://gitlab.com/matteoriato/informatica2122).  
Ronchin Tommaso [repo](https://github.com/TommasoRonchin/inf21-22).  
Rosso Simone [repo](https://gitlab.com/RossoSimo/Informatica2122).  
Rotunno Francesco [repo](https://gitlab.com/francesco.rotunno/inf-21-22).  
Sirbu Dumitru [repo](https://gitlab.com/dumitru.sirbu2/info-21-22).  
Varponi Leonardo [repo](https://gitlab.com/leonardo.varponi/Informatica21-22).  
Volpato ALberto [repo](https://gitlab.com/AlbertoVolpato/info21_22).




