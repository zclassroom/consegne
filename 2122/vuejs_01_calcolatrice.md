# calcolatrice

Usando VueJS (ver.3) ed un framework, ad esempio Bulma, si reaizzi una calcolatrice (non scientifica).

## Valutazione

Contribuiscono alla valutazione: la pulizia del codice, la documentazione e le scelte operate. Il progetto deve essere consegnato sulla cartella `calcolatrice` progetto (repository) `informatica` (o come è stato chiamato), la documentazione va nel file README.md. Le valutazioni eccellenti (8, 9, 10) sono riservate a coloro che daranno un consistente apporto personale e creativo con attenzione all'usabilità dell'applicazione. 
