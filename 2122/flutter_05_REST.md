# REST 

Opentriviadb [qui](https://opentdb.com/) è un sito web che attraverso apposite API permette di accedere ad un database di "quiz" trivia nei formati vero/falso o a riposta multipla. Le pagine del sito permettono di personalizzare l'uso alle API in modo da ottenere le domande nel formato e nelle quantità desiderati.

## Prima di ..

Familiarizzare con le API facendo test ad esempio con `curl` (installabile anche su Windows) o usando Firefox.

## il servizio

- Ogni utente è identificato da login e password (anche un account preesistente su Google).
- Gli utenti si iscrivono a squadre.
- Una squadra partecipa ad un match composto da 10 quiz (possibilmente gli stessi per tutti i suoi partecipanti).
- Un server REST - manda e riceve `json` - permette agli utenti di creare squadre, registrarsi nelle squadre ed entrare ed uscire dalle competizioni (login e logout), per realizzare tale server si può fare uso di [json-server](https://www.npmjs.com/package/json-server), per un approccio basic, oppure di un proprio server Node (KOa, Express ...), Java (Spring Boot) o altro linguaggio. Nel db legato a tale server si terrà traccia anche dei punteggi dei singoli utenti. Il punteggio delle squadre è dato derivato da informazioni già presenti nel server (somma punteggi iscritti appartenenti ad una singola squadra).
- Il Client è un app Flutter mediante la quale si possono accedere ai servizi di cui sopra e tentare le domande. 

## Valutazione

Contribuiscono alla valutazione: la pulizia del codice, la documentazione, presente e ben organizzata, e le scelte operate. Il progetto deve essere consegnato nella cartella `opentrivia` la quale contiene il server nella cartella `server` ed il client nella cartella `client` (app REST Flutter), la documentazione va nel file `README.md` in `opentrivia`, documentazione unica sia per server che per client. Le valutazioni eccellenti (8, 9, 10) sono riservate a coloro che:
- realizzeranno un server non usando `json-server`,
- realizzeranno l'autenticazione con Google,
- daranno un apporto personale e creativo con attenzione all'usabilità dell'applicazione.  



