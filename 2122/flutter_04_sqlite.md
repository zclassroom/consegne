# SQLite Database

Per questa consegna useremo ***Floor***, [qui](https://floor.codes/) l'home page, per la gestione di "oggetti persistenti" con db SQLite.

**App per note (memo)**  L'applicazione permette di gestire delle note (memo) in modo permanente 
- con un titolo ed un corpo (testo). 
- Sono previsti dei TAG, una nota può avere più TAG ed ad un TAG possono corripondere più note. 
- Una nota apparrtiene facoltativamente ad una categoria, una nota non può appartenere a più categorie. 
- TAG e categorie sono inseribili dall'utente.

**App MCU** L'app gestisce *Marvel Cinematic Universe*, vedi Wikipedia [qui](https://it.wikipedia.org/wiki/Marvel_Cinematic_Universe). Il database è presente come *dump* nel file [mcu.sql](mcu.sql).
Per prima cosa le tabelle del *dump* vanno importate in SQlite nell'applicazione; una possibilità è convertirle in file `.csv` per poi importarle in SQLite. Presumibilmente l'utente dell'app dovrà caricare il/i file `.csv`. Verrà preparato un *fronte end* di consultazione; a titolo di esempio di seguito vengono riportate alcuni dei possibili dati da visualizzare:
- lista dei film completa dei supereroi che appaiono in ciascun film,
- lista di tutti i film in cui appare un certo supereroe e
- lista dei film ordinata per data di uscita.


## Valutazione

Contribuiscono alla valutazione: la pulizia del codice, la documentazione e le scelte operate. Il progetto deve essere consegnato nella cartella `note` ovvero `mcu` nel progetto (repository) `TPSIT` (o come è stato chiamato), la documentazione va nel file `README.md`. Le valutazioni eccellenti (8, 9, 10) sono riservate a coloro che si dedicheranno alla seconda applicazione "app MCU", si terrà conto, come al solito per entrambe le app, dell'apporto personale e creativo con attenzione all'usabilità dell'applicazione.  




