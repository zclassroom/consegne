## cronometro o timer

Si realizzi un'applicazione Flutter per un cronometro ovvero un timer (conto alla rivescia) a secondi partendo dall'applicazione basic:  
- uno stream funge da *ticker* (un trigger di eventi) e viene "osservato" dall'applicazione
- timer o cronometro possono essere messi in pausa, stoppati ed azzerati.

## Valutazione

Contribuiscono alla valutazione: la pulizia del codice, la documentazione e le scelte operate con attenzione particlare all'uso degli stream. Il progetto deve essere consegnato sulla cartella `cronometro` ovvero `timer` nel progetto (repository) `TPSIT` (o come è stato chiamato), la documentazione va nel file `README.md`. Le valutazioni eccellenti (8, 9, 10) sono riservate a coloro che daranno un consistente apporto personale e creativo con attenzione all'usabilità dell'applicazione.  

