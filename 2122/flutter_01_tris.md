# TRIS man vs IA

Si realizzi il classico TRIS, la tabella 3x3 può essere costruita con bottoni. L'esercitazione ha scopo esplorativo sui *widget* di Flutter.

## Valutazione
Contribuiscono alla valutazione: la pulizia del codice, la documentazione e le scelte operate. Il progetto deve essere consegnato sulla cartella `tris` progetto (repository) `TPSIT`, la documentazione va nel file README.md. Le valutazioni eccellenti (8, 9, 10) sono riservate a coloro che daranno un consistente apporto personale e creativo con attenzione all'usabilità dell'applicazione. Contribuisce alla valutazione anche la realizzazione dell'IA (basic è l'IA che risponde a caso).
