# 5IAC AS 2021-2022

Leone Bacciu [repo](https://gitlab.com/LeoneBacciu/tpsit).  
Dario Caberlotto [repo](https://gitlab.com/Caberlotto/tipsit-5ic).  
Giacomo Guidotto [repo](https://gitlab.com/giacomo.guidotto/tpsit-projects).  
Tommaso Maglio [repo](https://gitlab.com/Sabtom/tpsit).    
Marco Mazzonetto [repo](https://gitlab.com/Marcomazzo/tpsit-5ic).  
Radu Mereacrii [repo](https://gitlab.com/radumereacrii/tpsit).  
Fabio Palma [repo](https://gitlab.com/FabioPalma03/tpsit_21-22).  
Filippo Pizzo [repo]( https://gitlab.com/Pizzox/tpsit-2021-2022).  
Alberto Pomiato [repo](https://gitlab.com/AlbertoPomiato/tipsit).   
Matteo Povelato [repo](https://gitlab.com/Matteo_Pove/tpsit-5ic-2021-22).  
Aron Prela [repo](https://gitlab.com/aron.prela/tpsit).   
Elia Stevanato [repo](https://gitlab.com/eliastevanato2003/tpsit-5ic).   
Giacomo Tanduo [repo](https://gitlab.com/giacomotanduo/tpsit).  
Federico Volpato [repo](https://gitlab.com/volpatof142003/TPSIT).  
Gabriele Zolla [repo](https://gitlab.com/gabriele.zolla/tpsit-2021).  
