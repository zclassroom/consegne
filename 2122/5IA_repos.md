# 5IA AS 2021-2022

Baruzzo Marco [repo](https://github.com/MarcoBaruzzo1/TPSIT_2021-22)  
Capo Simone [repo](https://github.com/Simocapo/TPSIT-2021-2022)  
Ciortan Radu [repo](https://github.com/CRadu1337/TPSIT-2021-22)  
Darie Marin [repo](https://github.com/darie12/TPSIT-2021-22)  
De Rossi Lorenzo [repo](https://github.com/lorenzoderossi/TPSIT-2021-22)  
Gobbo Michele [repo](https://github.com/MicheleGobbo4/TPSIT-2021-22)  
Livieri Alvise [repo](https://github.com/AlviseLivieri/TPSIT2021-2022).  
Mihalich Marco [repo](https://github.com/Micalich/TPSIT-2021-22)  
Pinzan Davide [repo](https://github.com/Davigamer20/tpsit2021-22)  
Prosdocimi Diego [repo](https://github.com/SaybeOwO/TPSIT-21-22)  
Rocchesso Marcello [repo](https://github.com/Celloroc/TIPSIT-2021-22)  
Sanna Gabriele [repo](https://github.com/GabrieleSanna003/TPSIT-21-22)     
Toure Mohamed [repo](https://github.com/Mohamedtoure00/Tpsit2021)  
Valtan Matteo [repo](https://github.com/MatteoValtan/TPSIT-21-22)  
Visentin Roberto [repo](https://github.com/RobertoVisentin33/TPSIT21-22)  
Zhu Luca [repo](https://github.com/Luca-Zhu/TIPSIT-2021-2022)   
