# Binary Search Tree

Si realizzi la classe `BSTree` in `Java` con un campo privato (matrice) del tipo 
```
[1, 2, 4, 5]      // posizione dei nodi: 1 radice, 2, 3 primo livello ....
[10, 20, 11, 17]  // valore dei nodi
```
la prima riga rappresenta la posizione dei nodi: `1` radice, i suoi figli al primo livello `2,3`, il secondo livello `4, 5, 6, 7` e così via.

l'albero contiene `15` nodi e va creato in modo *random* (come?)
- Si realizzi la classe.
- Il metodo di inserimento.
- Il metodo di camcellazione\*.
- Il metodo di ricerca nodo

## Valutazione
Contribuiscono alla valutazione: la pulizia del codice, la documentazione e le scelte operate. Il progetto deve essere consegnato sulla cartella `bSTree` nel progetto (repository) `Informatica` (o similare), la documentazione va nel file README.md. Le valutazioni eccellenti (8, 9, 10) sono riservate a coloro che realizzeranno \* e daranno un consistente apporto personale eventualmente aggiungento *feature*. Altro elemento di valutazione è la generazione *random* dell'albero, non alla portata di tutti!
