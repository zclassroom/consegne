# Visita di un BST

Per ogni STEP un commit.

## I step: classe `BTree`

Per realizzare la classe `BTree` , *Binary Tree*, internamente si usi un `ArrayList` di `Integer` -- struttura dati dinamica con la possibilità di usare valori `null` -- in modo che in posizione `0` vi sia la radice e, dato un nodo in posizione, `i` possiede gli eventuali figli, figlio sinistro e destro, rispettivamente in posizione `2i+1` e `2i+2` (in sostanza i nodi sono numerati da sinistra a destra a partire dalla radice scendendo sui livelli).  
- La struttura dati non presenta valori duplicati (in sostanza è un insieme), è facoltativo implementare l'interfaccia `Set`.
- Si realizzino il metodo: `int search(int val)` che ritorna la posizione del nodo dato un valore, `-1` indica valore non presente. E' facoltativo gestire il tutto con le eccezioni.
- Creare i metodi `int leftChild(int val)` e `int rightChild(int val)` per avere la posizione del figlio sinistro e destro: se `val` non fosse presente si ritorna `-2` se `val` è presente ma il figlio non c'è si ritorna `-1`. Di nuovo: è facoltativo gestire il tutto con le eccezioni.
- Altri metodi di utilità.

## II step: visite del `BTree`

Mediante uno *stack* realizzato come *nested class* o come altra classe esterna dall'utente, si visiti l`albero binario ( lo *stack* permette di evitare la ricorsione).
- Si realizzi il metodo di `BTree` `int[] preOrder()` che ritorna una collezione ordinata di valori presenti nella struttura. 

### III e IV step: la classe `BSTree`

La classe `BSTree`, *binary search tree*, estende `BTree`.
- Si realizzi il metodo `void insert(int val)` che inserisce il valore nel nuovo insieme, se `val` fosse presente il metodo non modifica la struttura.
- SI realizzi il metodo `void dell(int val)` che elimina il valore `val`, se `val` non fosse presente il metodo non modifica la struttura.

