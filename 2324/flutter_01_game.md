# game

Vengono proposte 3 possibilità.

1) **Tris** un classico senza tempo. Valutaziione: fino a `sufficiente/buono`.  
2) **Palora** un mix fra MasterMind ed un indovinello! Vedi App Store Android. Valutaziione: fino ad `eccellente`.  
3) **Campo Minato** un altro classico! Valutaziione: fino ad `eccellente`.  


## alcuni riferimenti

[1] "Basic Widhets" dalla documentazione ufficiale [qui](https://docs.flutter.dev/development/ui/widgets/basics).  
[2] "Cook Book" dalla documentazione ufficiale [qui](https://docs.flutter.dev/cookbook).  

## dead line

Entro le 23:59 del 29/10/2023

## valutazione

Contribuiscono alla valutazione: la pulizia del codice, la documentazione e le scelte operate. La cartella di progetto è `<nome_gioco>`, il progetto (repository) è `TPSIT` (o nome comunicato al docente), la documentazione va nel file `README.md`. Le valutazioni `ottimo` ed `eccellente` sono riservate a coloro che daranno un consistente apporto personale e creativo con attenzione all'usabilità dell'applicazione.
