# gestionale e prodotti

Realizzare una pagina Web che permetta di gestire una lista di prodotti per mezzo di tre bottoni:
- inserimento di un nuovo prodotto
- rimozione di un prodotto (per codice)
- ordinamento della lista.

Specifiche di progetto:
- utilizzare la struttura di progetto proposta
- servirsi delle istruzioni `prompt` e `alert` per interagire con l'utente
- un prodotto è definito da: codice, nome, descrizione, prezzo
- inserire opportuni controlli relativi alle specifiche operazioni (es.: non inserire lo stesso codice prodotto)

## nota

La consegna è prevista per una singola sessione di laboratorio.
