# web file viewer

Nella cartella `file-viewer` si crei un progetto Node usando `npm` per un server web con le seguenti caratteristiche.
- Tale cartella possiede i file: `app.js` - il server - e dei file di testo per il *testing*.
- Il server presenta un'unica pagina web, quindi siamo di fronte ad una *single-page-application*. Tale pagina possiamo "costruirla" utilizzando parte di codice `html` presente in alcuni file opportunamente approntati. Possibile l'uso di *framework* per lo stile. Alternativamente si può progettare il *client* in VueJS.
- Il server permette di elencare - un menù - e visualizzare il contenuto di ogni singolo file di testo `.txt` presente nella cartella `file-viewer`.
- Gli *handler* sono del tipo `localhost:3000/miofile.txt`, dando sul *browser* un tale URL viene "mostrato" il file `miofile.txt` (si dia uno sguardo nelle API all'oggetto *request* per ottenere `URL` e quindi a [3]). 


## riferimenti

[1] Il modulo `fs` [API](https://nodejs.org/api/fs.html).  
[2] "Getting Started" [qui](https://nodejs.org/en/learn/getting-started/introduction-to-nodejs) dalla documentazione ufficiale di Node.   
[3] Il modulo `URL` [API](https://nodejs.org/api/url.html).  
[3] esempi su "zuccante serc".  
