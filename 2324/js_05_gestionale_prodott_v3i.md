# gestine prodotti, terza versione VueJS e flex model.

Duplicare il progetto Javascript precedente utilizzando una form.

## form di inserimento prodotti 

il form è utilizzato per richiedere/modificare i dati associati al prodotto (vedi passo successivo).
I dati in questione sono:
- categoria (menu a tendina)
- codice alfanumerico (input text, 6 caratteri)
- nome (input text)
- descrizione sintetica (input text)
- prezzo (input text)
- sconto (radio button, sceglie fra varie percentuali)
- in magazzino (check box: Si/No)
- bottone per confermare i dati (viene creato/modificato l'oggetto corrispondente che viene caricato nell'array).

## visualizzazione prodotti 

i prodotti vengono visualizzati (nella stessa pagina dopo il form o in un'altra se si è in grado di farlo) in una lista, per ogni elemento della lista un bottone permette di visualizzare il dettaglio ovvero ripresentare il precedente form compilato per una possibile modifica, un altro bottone permette la cancellazione del singolo prodotto.

## NOTA

Il progetto, in questa versione, viene fatto in JS vanilla (no Vue JS).
