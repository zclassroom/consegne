# Mock-server

**NB** Consegna in preparazione della verifica pratica.

Creare una applicazione che permette di selezionare da un menù a tendina il cognome di una persona e visualizzi i dati anagrafici.
I dati anagrafici sono forniti da mock server e salvati nel file db.json.
All'avvio dell'applicazione impostare una prima richiesta al mock server per farsi dare l'elenco dei cognomi da caricare nella tendina.
Per farsi dare i dati anagrafici della persona, effettuare ogni volta una richiesta al server (quindi non una richiesta globale all'inizio).

