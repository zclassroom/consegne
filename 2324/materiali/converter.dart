import 'package:http/http.dart' as http; //fetch http

import 'dart:convert';
//importo json

//aggiungere a AndroidManifest.xml i permessi per accedere ad internet
//  <uses-permission android:name="android.permission.INTERNET" />

//aggiungere a pubspec.yaml il pacchetto http
//http:

//lanciare il comando Flutter pub get
//in alternativa $flutter pub add http

main() {
  var risposta = provaFuture().then(provaFuture2);
  //esecuzione asincrona dei due metodi.
  //quando il primo termina il risultato viene passato al secondo metodo
  print(risposta);

  provaFuture().then(provaFuture2);

  var p = restituisciPromise().then(restituisciPromise2);
  //i due metodi sono eseguiti in modo asincrono. Quando il primo termina
  //viene passato il risultato al secondo metodo
  print("stampa:  ${p}");
}

provaFuture() {
  return http.get(Uri.parse("lib/dati.txt"));
  //restituisce un oggetto Future
}

provaFuture2(risposta) {
  if (risposta.statusCode == 200) {
    print("stato OK");
    print(risposta.body);

    print("--------------------------------");
    var rispostaJson = json.decode(risposta.body);
    //la stringa contenente dati in formato json è trasformata in oggetto
    print(risposta);

    print(rispostaJson);
    print(rispostaJson is List);
    print(rispostaJson[0]["nome"]);

    var s;
    s = json.encode(rispostaJson);
    //questo trasforma un oggetto in una stringa in formato json
    print(s);
    print(s is String);

    return risposta.body;
  } else {
    print("errore fetch dei dati");
  }
}

Future restituisciPromise() {
  var promise = Future.value("bezkoder");
  return promise;
  //il metodo restituisce un Future creata a partire da una stringa
}

restituisciPromise2(risposta) {
  print(risposta);
  return risposta;
}
