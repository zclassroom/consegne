# Progetto interdisciplinare - client REST

Per TPSIT ci si concentra sulla documentazione e sulla creazione del *client REST*  in Flutter.

## deadline

- 5IA: 23:59 13/05/2024
- 5IB: 23:59 12/05/2024
- 5IC: 23:59 08/05/2024

(inizio maggio)

## fasi intermedie

- Documentazione con Swagger e OpenApi: metà aprile.

## la consegna 

Per il servizio o parrte di esso messo in essere ad Informatica si realizzi *endpoint* REST (json) per un *client mobile* Flutter CRUD!

## valutazione

Contribuiscono alla valutazione:
- il rispetto delle consegne (tempi e richieste),
- la pulizia del codice e
- la documentazione e le scelte operate. 

Per il progetto (nome a piacere) si crei un *repository* in GitHub o GitLab, la documentazione va nel file `README.md` e nel fila `.yaml` per le Open Api, tale repository conterrà, su cartelle separate, il *server* ed il *client*, la documentazione è esterna a tali cartelle.

### valutazione sufficiente (max 6)

- **server REST**: realizzabile con *mock server* tipo `json-server` (Node) o altro Php.
- **serializzazione e deseriualizzazione**: `json`diviene `Map` (deserializzazione).
- **CRUD completo** (salvo accordo col docente da attuarssi all'inizio del progetto).

### valutazione più che sufficiente (max 6.5)

- **server REST**: dal proprio progetto di Informatica, vengono esposti gli *endpoint*.
- **serializzazione e deseriualizzazione**: `json`diviene `Map` e `Map` diviene `Model` mediante classe autogenerata (deserializzazione): esistono appositi pacchetti Flutter o traduttori nel web.
- **CRUD completo**.

### valutazione buona (max 7)

- **server REST**: dal proprio progetto di Informatica, vengono esposti gli *endpoint*.
- **serializzazione e deseriualizzazione**: `json`diviene `Map` e `Map` diviene `Model` mediante classe autogenerata (deserializzazione): esistono appositi pacchetti Flutter o traduttori nel web.
- **DB interno**: il `Model` viene usato per conservare in modo persistente le informazioni mediante DB interno (SQLite con ORM, vedi `Floor`, o ObjectBox o similari).
- **CRUD completo**. 

### valutazioni eccellenti (max 8, 9, 10)

- **server REST**: dal proprio progetto di Informatica, vengono esposti gli *endpoint*.
- **serializzazione e deseriualizzazione**: `json`diviene `Map` e `Map` diviene `Model` mediante classe autogenerata (deserializzazione): esistono appositi pacchetti Flutter o traduttori nel web.
- **DB interno**: il `Model` viene usato per conservare in modo persistente le informazioni mediante DB interno (SQLite con ORM, vedi `Floor`, o ObjectBox o similari).
- **CRUD completo**. 
- **Business Logic** con uso di pacchetti per gestiona dello stato (minimo `Provider`).

Tali valutazioni sono riservate a coloro che daranno un consistente e consapevole apporto personale e creativo con attenzione all'usabilità dell'applicazione.
