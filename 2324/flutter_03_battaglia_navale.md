# Battaglia navale

Si devono realizzare:
- Server TCP o web socket in Dart o Java (no PhP).
- Un client testuale TCP o web socket in Dart (a riga di comando).
- Un client mobile TCP o web socket in Flutter.

## materiali

[1] "Set up Android Emulator networking" [qui](https://developer.android.com/studio/run/emulator-networking?hl=en).

## dead line

Entro le 23:59 del 03/12/2023

## valutazione

Contribuiscono alla valutazione: la pulizia del codice, la documentazione e le scelte operate. La cartella di progetto è `battaglia_navale` con altre tre cartelle (`server`, `client` ed `app_battaglia_navale` per il client mobile), il repository è `TPSIT` (o nome registrato), la documentazione va nel file `README.md` dentro `battaglia_navale` eventuali file `README.md` nelle singole cartelle. Le valutazioni eccellenti (`8`, `9`, `10) sono riservate a coloro che daranno un consistente e consapevole apporto personale e creativo con attenzione all'usabilità dell'applicazione.
