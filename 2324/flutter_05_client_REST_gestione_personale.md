# Consegna Flutter

Creare usando le risorse nella cartella `materiali`, file `converter.dart`, server `server.php` (avviabile con `php -S` ovvero `LAMP` ovvero `LiveServer` estensione di VSC ).

## deadline

- 5IA: 23:59 12/03/2024
- 5IB: 23:59 04/03/2024
- 5IC: 23:59 18/03/2024

## la consegna (prof. D.Dentico)

Creare una applicazione in flutter che realizzi un Client REST.
L'applicazione deve interfacciarsi con il server REST richiedendo i dati di un dipendente dell'azienda. 
La richiesta viene effettuata tramite protocollo HTTP utilizzando il metodo get per passare il parametro 'codice'.
L'applicazione è composta, come visto a lezione, da due schermate: 
Nella prima schermata è presente un pulsante per passare alla seconda schermata.
La seconda schermata presenta una casella di testo e un pulsante per inviare la richiesta al server.
I dati ricevuti dal server sono codificati in formato JSON.
E' necessario effettuare il parsing in modo da convertirli in oggetti Dart, e poi visualizzarli all'utente.



## valutazione

Contribuiscono alla valutazione:
- il rispetto delle consegne (tempi e richieste),
- la pulizia del codice e
- la documentazione e le scelte operate. 

La cartella di progetto (nome a piacere) sta in `TPSIT` (o nome registrato per il *repository*), la documentazione va nel file `README.md`. 

Le valutazioni eccellenti (`8`, `9`, `10) sono riservate a coloro che daranno un consistente e consapevole apporto personale e creativo con attenzione all'usabilità dell'applicazione.
