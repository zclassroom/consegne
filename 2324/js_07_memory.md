# gioco del Memory

Il gioco è noto.
- Il campo è rettangolo.
- Vi sono varie difficoltà da definire in fase di inizializzazione.
- Le immagini sono prese con le [API](https://developers.thecatapi.com/view-account/ylX4blBYT9FaoVd6OhvR?report=bOoHBz-8t) in modo casuale in fase di inizializzazione. Usare ad esempio `fetch`.
- Usare le funzioni asincrone `init()` e `play()`.
- Curare usabilità, Vue JS è facoltativo.


