# JS e Canvas

Per ogni STEP un TAG annotato, [qui](https://gitlab.com/divino.marchese/zuccante_src/-/wikis/git/survivor) alcuni appunti.

## I step "calcolatrice con vanilla js" [dead line 240208]

TAG: `v0.1`

Il progetto `calcolatrice` possiede due cartelle `calcolatrice_js` e `calcolatrice_vue`. Usando un *framework* CSS - Bootstrap ad esempio - si crei una calcolatrice standard (non scentifica), i file sono `index.html`, `style.css` e `app.js`;
non è consentito l'uso di librerie esterne, solo "JS vanilla". Il detaglio di questa prima parte è [qui](https://classroom.google.com/w/NjIyOTQwMTQ1MTMy/t/all).

Intestazione `h1` "Nome Cognome - Calcolatrice".

## II step "calcolatrice con Vue JS" [dead line dopo Carnevale]

TAG: `v0.2`

Si lavori nella cartella `calcolatrice_vue`, si usi un *framework* CSS - Bootstrap o Boolma - e si crei una calcolatrice standard (non scentifica) usando `Vue JS 3`, i file sono `index.html`, `style.css` e `app.js`.

Intestazione `h1` "Nome Cognome - Calcolatrice".

In `calcolatrice` una breve relazione `RELAZIONE.md` in cui si danno le proprie considerazioni sui due approcci (pro e contro ecc.).

## nota

Non usare `eval` (permette di valutare una stringa come espressione matematica semplice).
