# JS e Canvas

Per ogni STEP un branch (commit a piacere)

## I step "gestione degli urti"

Il progetto `palle_pazze` - creare l'omonima cartella - contiene i file `index.html`, `app.js`, `style.css` e `README.md` per la documentazione.

Intestazione `h1` "Nome Cognome - Palle Pazze"

Un *canvas* individua un'area rettangolare nello schermo (larghezza `w` ed altezza `h` a piacere) con orientamento *landscape* (altezza più piccola della larghezza) e colore di sfondo neutro chiaro. Su tale area si posizionino `4` cerchi di raggio a piacere nelle posizioni `(1/4w, 1/4h)`, `(3/4w, 1/4h)`, `(1/4w, 3/4h)` e`(3/4w, 3/4h)` (ascissa ed ordinata sono frazioni della larghezza `w` e dell'altezza `h` dell'area rettangolare), il colre dei cerchi è: rosso, giallo, verde e blu, si usi rgb.

I cerchi partono con velocità *random* a modulo costante inizializzato nel codice scritto in `app.js` (si suggerisce l'uso delle funzioni trigonometriche). Rimbalzano in modo elastico (come il biliardo) fra loro e sulle pareti.

## II step "merge dei colori e bottone di avvio"

Quando si scontrano due palle esse mescolano il loro colore, ad esempio giallo e rosso se si scontrano diventano arancione; è opportuno progettare una funzione `mergeColor` che mescola i colori espressi in formato rgb. SI inserisca inoltre un bottone che avvia l'applicazione o la resetta posizionando i cerchi come indicato nello step precedente e dando i `4` colori in modo *random*. Per i più volenterosi, valutazioni eccellenti: si dia la possibilità all'utente di inserire il valore del modulo della velocità al momento del riavvio dell'applicazione. 

