
# timer o cronometro

Si realizzi un timer od un cronometro.  Uno *stream* lavora come un oscillatore generando regolarmente un evento di *tick*: evitare frazioni di secondo al di sotto del tempo di *refresh* di Flutter. Un altro *stream*, ottenuto dal precedente, permette di ottenere dal *tick* il tempo da presentare in ore, minuti e secondi. Si usi opportunamente una `StreamSubscruiption` per gestire il timer/cronometro.

## dead line

Entro le 23:59 del 05/11/2023

## valutazione

Contribuiscono alla valutazione: la pulizia del codice, la documentazione e le scelte operate. La cartella di progetto è `timer` o `cronometro`, il progetto (repository) è `TPSIT` (o nome comunicato ai docenti), la documentazione (piccola relazione) va nel file `README.md`. Le valutazioni `ottimo` ed `eccellente` sono riservate a coloro che daranno un consistente apporto personale e creativo con attenzione all'usabilità dell'applicazione.
