# Consegna Floor

Per ogni STEP un TAG annotato, [qui](https://gitlab.com/divino.marchese/zuccante_src/-/wikis/git/survivor) alcuni appunti.


# Sep I [dead line 240209]

TAG: `v0.1`

Si crei usando `Floor`, [home](https://pub.dev/packages/floor), una app `Flutter` con `SQLite` per salvare in modo persistente `Post` e `Comment` (le due *entity*), ogni commento deve corripsondere ad un post, le proprietà delle *entity* sono lasciate alla libertà di progetto, presente deve essere la chiave primaria `id`. In questa prima fase si imposti il DB e sifaccia un minimo di test mediante *logging*.

# Sep II [dead line 240214]

TAG: `v0.1`

Si comleti gestendo nel DB la cancellazione dei post e, conseguentemente, quella dei relativi commenti finalizzando l'interfaccia grafica.

## valutazione

Contribuiscono alla valutazione:
- il rispetto delle consegne (tempi e richieste),
- la pulizia del codice e
- la documentazione e le scelte operate. 

La cartella di progetto (nome a piacere) sta in `TPSIT` (o nome registrato per il *repository*), la documentazione va nel file `README.md`. 

Le valutazioni eccellenti (`8`, `9`, `10) sono riservate a coloro che daranno un consistente e consapevole apporto personale e creativo con attenzione all'usabilità dell'applicazione.
