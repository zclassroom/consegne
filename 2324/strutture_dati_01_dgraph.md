# Clase `DGraph`

SI realizzi la classe `DGraph` per un grafo orientato a partire dalla sua matrice delle adiacenze.
- Il costruttore, i *getter* ed i *setter* sono rischiesti (inserimento di un arco, verifica della presenza di un arco ecc.).
