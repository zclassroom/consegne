# Server e client REST

Il server REST `json` in `Fastify` (framework `node`) prevede l'uso di un DBMS
- `SQLite` + `ORM`
- `PostgreSQL`
Client web (con framework JS anche Vue) o mobile.

## dead line

Entro le 23:59 del 08/01/2023

## valutazione

Contribuiscono alla valutazione: la pulizia del codice, la documentazione e le scelte operate. La cartella di progetto è `db_01`, tale cartella sarà consegnata su di un repository `GitHub` o `GitLab`, una cartella per il client ed una per il server, la documentazione, sia nel client che nel server, va nel file `README.md`. Le valutazioni eccellenti (8, 9, 10) sono riservate a coloro che daranno un consistente apporto personale e creativo con attenzione all'usabilità dell'applicazione.
