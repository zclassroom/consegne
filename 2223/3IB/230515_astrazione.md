# esercitazione sulle classi astratte ed interface 

Realizzare il seguente scenario decidendo di volta in volta se usare interfacce, classi astratte o classi (concrete) e scegliendo strutture di dati statiche, leggi array, o dinamiche, leggi liste. Ricordiamo che classi ed interfacce in Java definiscono un tipo, genericamente quindi parleremo di tipo. 

Ci riferiamo ad una struttura residenziale (un condominio ad esempio): definire la classe `Residenza`. 
- Una `STruttura` ha `5` piani: definire il tipo `Piano`.
- Ogni piano ha `10` appartamenti: definire il tipo `Appartamento`.
- Ogbni `Appartamento` ha un certo numero di stanze, definire il tipo `Stanza`.
- Si possono dare solo stanze del seguente tipo: `Cucina`, `Bagno`, `Salotto`, `Disimpegno` e `Camera`.

Simulare quindi la generazione di una `Residenza` con un suo metodo *factory* chiamato in una classe `ResidenzaTest` limitandosi ad appartamenti che abbiamo da `4` ade `8` stanze.

- 

