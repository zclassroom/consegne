public class LambdaTest2 {

        public static void main(String[] args) {

        LambdaTest2 test = new LambdaTest2();
        String myString = "bla bla bla";
        Getter getter = () -> myString;
        System.out.println(test.coolMethod(getter));
        // myString = "maramao";
    }

    public String coolMethod(Getter g) {
        return g.get();
    }

}

@FunctionalInterface
interface Getter {
    String get();
}
