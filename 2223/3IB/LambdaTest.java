public class LambdaTest {

    String msg = "test message";
    Getter getMsg = () -> msg; // very cool

    public static void main(String[] args) {

        LambdaTest test = new LambdaTest();
        System.out.println(test.coolMethod(test.getMsg));
    }

    public String coolMethod(Getter g) {
        return g.get();
    }

}

@FunctionalInterface
interface Getter {
    String get();
}
