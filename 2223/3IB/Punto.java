public class Punto {

    // ascissa
    private double x = 0.0f;
    // ordinata
    private double y = 0.0f;

    // origine degli assi
    public static final Punto origin = new Punto(0.0d, 0.0d);

    public double getX(){ return 0.0d; }
    public double getY(){ return 0.0d; }
    public void setX(){}
    public void setY(){}

    // costruttore senza parametri
    public Punto(){}
    // costruttore con due parametri
    public Punto(double x, double y){}

    // distanza fra due punti
    public static double distanza(Punto p1, Punto p2){ return 0.0d;}

    @Override
    public String toString() { return ""; }
        
}
