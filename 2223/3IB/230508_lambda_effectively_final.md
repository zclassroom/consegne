# esercitazione sulla sintassi lambda

Si studi nel dettaglio (provarndolo) il seguente sorgente
```java
public class LambdaTest2 {

        public static void main(String[] args) {

        LambdaTest2 test = new LambdaTest2();
        String myString = "bla bla bla";
        Getter getter = () -> myString;
        System.out.println(test.coolMethod(getter));
        // myString = "maramao";
    }

    public String coolMethod(Getter g) {
        return g.get();
    }

}

@FunctionalInterface
interface Getter {
    String get();
}
```
Una variabile è detta *effectively final* se, come `final` viene inizializzata e non più modificata successivamente (non necessita della parola chiave *final*).
- Cosa accade se si decommenta la linea?
- Compilando il sorgente si ottiene il file `.class` per la classe anonima corrispondente all'oggetto lambda?
-  Si può (provare) a riscrivere il tutto con classe anonima che implementa la *functiona interface*?