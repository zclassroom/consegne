# Timer

Inteso come "conto alla rovescia". A differenza della precedente consegna ci si concentra sulla gestione degli `Stream` e della `StatefulWidget`, l'interfaccia dovrebbe risultare semplice. 
- E`richiesta la gestione della *subscription* degli *stream*. 
- Lo studente realizzerà un opprotuno `StreamController` che, come un oscillatore, genererà i *tick* (eventi di trigger).
- Lo *stream* di *tick* servirà per gestire, possibilmente trasformandolo in un altro *stream* di numeri, l'avanzamento del conto alla rovescia.
- Pausa, stop e scelta della partenza - minuti e secondi - sono obbligatori.

## alcuni riferimenti

[1] API varie di Dart su `Stream` e `STreamController`.

## dead line

Entro le 23:59 del 19/11/2022

## valutazione
Contribuiscono alla valutazione: la pulizia del codice, la documentazione e le scelte operate. La cartella di progetto è `timer`, il repository è `TPSIT`, la documentazione va nel file `README.md` (dentro la cartella di progetto). Le valutazioni eccellenti (8, 9, 10) sono riservate a coloro che daranno un consistente apporto personale e creativo con attenzione all'usabilità dell'applicazione.
