# Chatroom

Una stanza di discussione cui ci si connette come utenti e si inviano messaggi; i messaggi vengono visti da chiunque sia connesso alla stanza di discussione; ogni messaggio riporta un testo massimo di 126 caratteri, la data e l'ora ed il nome dell'utente.
- Server TCP/web socket in Dart oppure in Node o con altri linguaggi ineterpretati (no Php, comunicare col docente).
- Un client mobile in Flutter ed un client testuale a riga di comando ovvero sito web con Node nel caso si sia scelto di fare il server con framework Node.

## dead line

Entro le 23:59 del 06/01/2023

## valutazione

Contribuiscono alla valutazione: la pulizia del codice, la documentazione e le scelte operate. La cartella di progetto è `chatroom` con altre tre cartelle (server, client mobile e client testuale), il repository è `TPSIT`, la documentazione va nel file `README.md` (dentro la cartella di progetto). Le valutazioni eccellenti (8, 9, 10) sono riservate a coloro che daranno un consistente apporto personale e creativo con attenzione all'usabilità dell'applicazione.
