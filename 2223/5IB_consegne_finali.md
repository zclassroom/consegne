# consegne finali 5IB

Consegne finali di fine anno.

## ronchin 

- **nome servizio**: app Refill
- **server**: Fastify + Ppstgres, con admin e diversi permessi e il cliente, usa toke jwt, gestisce reset password e email, OAUTH2 per autenticazione
- **client**: OenSTreetMap e GoogleMaps, si userà QRCode satellite ecc
- **altro**: interessante il pannello admin con grafico accessi, statistiche refill, news, informazioni sugli utenyi
- **giudizio**: lavoro eccellente ed estremamente articolato
