# Memo

Partendo da **am023_todo_list** usare un *pattern* di centralizzazione dello stato tipo *provider*, *riverpod* o *redux* anche *block* per centralizzare il *DAO*.

## dead line

Entro le 23:59 del 25/02/2023

## valutazione

Contribuiscono alla valutazione: la pulizia del codice, la documentazione e le scelte operate. La cartella di progetto è `memo`, il repository è `TPSIT`, la documentazione va nel file `README.md` (dentro la cartella di progetto). Le valutazioni eccellenti (8, 9, 10) sono riservate a coloro che daranno un consistente apporto personale e creativo con attenzione all'usabilità dell'applicazione nel rispetto severo delle specifiche.
