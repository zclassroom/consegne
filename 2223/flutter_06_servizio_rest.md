# servizio REST

Questo è un progetto consuntivo.

## Server REST con CRUD 

- Tecnologie possibili: PhP, anche framework, NodeJs, Ruby on Rails, Python. Possibile anche Json-Server (vedi poi valutazione).

## Client

Client in Flutter.
- Gestione centralizzata dello stato,
- DB interno.
- Eventualmente gestione delle chiamate HTTP su di un isolate separato (scelta che va motivata).

## dead line

Entro le 07:00 del 15/04/2023

## valutazione

Contribuiscono alla valutazione: la pulizia del codice, la documentazione, il rispetto puntuale delle consgne, e le scelte operate. La cartella di progetto è `rest` (tutto minuscolo), il repository è `TPSIT` (o alternativo) su GitHub o GitLab, tale cartella contiene le due cartelle per client e server.
- `client` per il client mobile.
- `server` col server.
la documentazione comune, de clinet e del server va in uno o più file `README.md`. Vale quanto segue.
- Valutazioni (9, 10): oltre alle specifiche viene gestita l'uatenticazione in modo sicuro (Oauth2, Bcript ecc.. )
- Valutazioni fino al 7: chi usa Json-server.
Nella valutazione si terrà conto dell'apporto personale e creativo con attenzione all'usabilità dell'applicazione sempre nel rispetto severo delle specifiche.
