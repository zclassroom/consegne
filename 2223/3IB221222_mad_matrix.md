# Mad matrix

La classe `MadMatrix` rappresenta una matrice (quadrata) di `int`, essa ha 
- un solo campo, un *array* che contiene i valori distinti ed ordinati (ordine crescente),
- ad ogni valore è associato un *array* di coppie di interi, ogni coppia rappresenta la posizione del valore.

Il costruttore riceve una matrice `int[][]`. Il metodo `public toSTring()` lavora nel modo seguente: data una matrice
```
|1, 2, 4|
|4, 5, 1|
|8, 2, 4|
```
la conversione a `String` ritorna
```
1 -> (0, 0) (1, 2)
2 -> (0, 1) (2, 1)
4 -> (0, 2) (1, 0) (2, 2)
5 -> (1, 1)
8 -> (2, 0)
```
In una classe `MadMatrixTest` si provi una matrice quadrata, almeno `4x4`, con valori casuale, si userà un oggetto `Random`.

