# Memo

Centralizzando lo stato sul *dao* (vedi poi) si realizzi, usando SQLite e `floor` (ORM per SQLite), una applicazione di nome `journey` che permetta di registrare quanto segue.
- `Trip` ovvero percorsi fatti da `Stop` e 
- `Stop` ovvero singole fermate.
Ogni `Trip` è definito da una lista di `Stop` ha un nome e la data dell'ultimo aggiornamento. Ogni `Stop` possiede le due coordinate geografiche, un nome ed eventualmente altre informazioni. Chi utilizza l'app visualizza i `Trip` e per ogni `Trip` i dettagli degli `Stop`. Fra `Trip` e `Stop` vi è un legame molti a molti. 

## dead line

Entro le 23:59 del 25/02/2023

## valutazione

Contribuiscono alla valutazione: la pulizia del codice, la documentazione e le scelte operate. La cartella di progetto è `journey`, il repository è `TPSIT`, la documentazione va nel file `README.md` (dentro la cartella di progetto). Le valutazioni eccellenti (8, 9, 10) sono riservate a coloro che daranno un consistente apporto personale e creativo con attenzione all'usabilità dell'applicazione nel rispetto severo delle specifiche.
