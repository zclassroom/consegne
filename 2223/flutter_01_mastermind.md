# MasterMind

Si tratta di un gioco classico (si può cercarlo su Wikipedia): al posto dei 6 colori si possono usare i numeri da 1 a 6

![mm](https://vroegert.nl/wp-content/uploads/2017/12/Mastermind.jpg)

Il gioco può avere termine dopo un numero fissato di tentativi, si può introdurre anche un punteggio.

## alcuni riferimenti

[1] "Basic Widhets" dalla documentazione ufficiale [qui](https://docs.flutter.dev/development/ui/widgets/basics).  
[2] "Cook Book" dalla documentazione ufficiale [qui](https://docs.flutter.dev/cookbook).  

## dead line

Entro le 23:59 del 28/10/2022

## valutazione
Contribuiscono alla valutazione: la pulizia del codice, la documentazione e le scelte operate. La cartella di progetto è `mastermind`, il progetto (repository) è `TPSIT`, la documentazione va nel file `README.md`. Le valutazioni eccellenti (8, 9, 10) sono riservate a coloro che daranno un consistente apporto personale e creativo con attenzione all'usabilità dell'applicazione.
