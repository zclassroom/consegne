# Test su algoritmi di ordinamento

Il progetto IDEA chiamato `sorting` propone lo studio delle *performance* di alcuni algoritmi di ordinamento per
oggetti del tipo `int[]`.

## la classe `Utils`

Si progetti la classe
```java
public class Utils() {

    private static final Random gen = new Random(); 

    private Utils(){};

    public static int[] getRandomArray(int size) {
        // un factory method per un array random con valori da 1 a 100
    }

    public static void shuffleArray(int[] array) {
        // mescola l'array 
    }

}
```

## il tempo

Useremo una procedura un po' grezza ma efficace per il calcolo:
```java 
long start = System.currentTimeMillis();
doSomething();
long end = System.currentTimeMillis();
System.out.println("elapsed time: " + (end - start));
```
L'output a terminale è solo per proporre la differenza dei tempi.

## le valutazioni delle performance

Su `100` test fatti su un input casuale delle stesse dimensioni (ad esempio `13`) - ci riferiamo agli *array* - calcoleremo:
- tempo medio e
- deviazione standard  

In tal modo confronteremo gli algoritmi di seguito proposti. Le `100` misure verranno fatte nel modo seguente. Prepariamo un interfaccia
```java
public interface Sorter {
    void sort(int[] array);
}
```
La classe di `Test` è pensata per eseguire il test
```java
public class Test {

    private final Sorter sorter;

    public Test(Sorter sorter) {
        // code
    }

    public int runSingleTest(int[] array) {
        // return time ms test 
    }

    public runTest() {
        // ....
    }
}
```

## Bubble Sort

```java
public class BubbleSort implements Sorter {

    @Override
    public void sort(int[] array){
        // code 
    }
}
```
L'algoritmo vede due iterazioni (una interna ed una esterna):  
- data la posizione `i` non negativa alla fine dell'iterazione interna viene portato in tale posizione il minimo fra i 
valori che si trovano in posizione `j` dove `j` è maggiore o eguale ad `i`;
- la precedente iterazione viene ripetuta da `i` pari a `0` fino a ....

## Merge Sort

```java
public class MergeSort implements Sorter {

    @Override
    public void sort(int[] array){
        // code 
    }
}
```
L'algoritmo tipicamente procede in modo ricorsivo con tecnica *divide et impera*:
- dividi in due l'*array*,
- ordina la prima parte in modo crescente
- ordina la seconda parte in modo crescente e
- il passo `0` della ricorsione è costituito da due elementi di cui, eventaulemnte, per ordinarli dal più piccolo al più grande si fa lo *swap*.

Noi lavoreremo in modo iterativo con una serie di iterazioni:
- ordina, *sort*, le coppie di elementi adiacenti, se l'array è di dimensione dispari ...
- fondi, *merge*, le coppie adiacenti in modo da ottenere quadruple ordinate,
- fondi le quadruple adiacenti in modo da ....
- ...
- l'*array* infine è ordinato!

Useremo i seguenti due metodi (potremmo proporli anche `static`). Il primo è un semplice `swap`
```java
private void swap(int[] array, int i, int j) {
    // code
}
```
il metodo lavorerebbe con `i` pari ma soprattutto, se fatto bene, può lanciare una eccezione! Infine il metodo
```java
private void merge(int[] array, int start, int size) {
    // code
}
```
con cui vengono fusi fra loro, a  partire dalla posizione `i`, blocchi di elementi di dimensione pari a `size` ciascduno (il secondo blocco potrebbe essere di dimensioni inferiori se il nostro *array* termina), ad esempio
```java
merge(array, 0, 2);
```
fonde le prime due coppie di `array` a partire dalla posizione `0`, ecco una possibilità: sia il nostro *array*
```
[2, 7, 1, 5 ... ]
```
le coppie sono `[2, 7]` e `[1, 5]` e l'*array* diventa
```
[1, 2, 5, 7 ... ]
```

## La classe `Main`

Propone il confronto dei due algoritmi con dei `Test`.

