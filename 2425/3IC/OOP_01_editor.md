# editor: dizionario e correttore ortografico

Della seguente consegna si danno due versioni una facile ed una di mesia difficoltà. 
- **MD**: file da correggere e dizionari stanno in file di testo da leggere e scrivere usando `BufferedReader` e `BufferedWriter`.
- **F**: usare per testo da correggere e dizionari oggetti di tipo `List<String>`.

## pattern

Usando il *pattern* *IOC* - *Inversion Of Contrl* - si realizzi e si implementi una interfaccia `Editor` che è in grado di correggere un testo basandosi su di un oggetto di tipo (classe) `Dictionary`.
Ogni parola del testo verrà cercata usando la **ricerca binaria** nel dizionario ordinato in modo lessicografico ed il metodo `int analize(Text text)`, il correttore, ritorna il numero di parole
non presenti nel dizionario prescelto per una data implementazione dell'editor  su di un dato `Text`. Si realizzino almeno due dizionari che contengono un numero di almeno `100` parole
cadauno, ogni parola sia da `3` a `6` caratteri. Si testi il correttore su almeno un `Text` per almeno due implementazioni di `Editor` che si riferiscono ad almeno due dizionari (vedi sopra).

## materiali

[1] Per *IOC* si vedano gli appunti del docente.
[2] L'algoritmo di ricerca binaria si trova in Wikipedia.

## dead line

Entro le 23:59 del 25/02/2025.

## valutazione consegne

Il progetto `editor` è un progetto IDEA con Gradle (Kotlin) e si trova nel *repository* dello studente con l'opportuna documentazione nel file `README.md`,
per ogni sessione di laboratorio vi sarà un *commit* di fine sessione in cui si presenta il lavoro provvisorio, altri *commit* fatti non durante le sessioni di laboratorio sono ammessi.

Contribuiscono alla valutazione: il rispetto delle consegne, la pulizia del codice, la documentazione e le scelte operate. Le valutazioni `ottimo` ed `eccellente` sono riservate a coloro che daranno un consistente 
apporto personale e creativo con attenzione all'usabilità dell'applicazione, e che si dedicheranno alla versione **MD**. 
