public class ZFibonacci {

    public ZFibonacci(int n) {
        // il costruttore per oggetti generatori dei numeri di Fibonacci
    }

    public int[] forFibonacci() {
        // return n numeri di Fibonacci usando ciclo for
        return new int[10];
    }

    public int[] whileFibonacci() {
        // return n numeri di Fibonacci usando ciclo while
        return new int[11];
    }

    public int[] rFibonacci() {
        // return n numeri di Fibonacci usando la ricorione
        return new int[12];
    }

    public int[] stackFibonacci() {
        // return n numeri di Fibonacci usando stack (refactoring ricorsione)
        return new int[13];
    }

    public String toString() {
        // ritorna con n = 5: "sono un generatore di 5 numeri di fibonacci"
        // NB usare stringhe formattate
        return "sono un generatore di 10 numeri di Fibonacci";
    }
}