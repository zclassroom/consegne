# broadcast

- **deadline 23:59 26/03/2025**

Proponiamo due possibili consegne da inserire, almeno una, nel progetto IDEA, il nome del progetto potrà essere
- `broadcast_network` o
- `Broadcast_chatrrom`

Useremo come base il progetto `events` [1].

# network (MD)

Un `Server` comunica a dei `Client` registrati con il loro indirizzo - una stringa alfanumerica di `4` caratteri.
Vengono inviati in modo *broadcast* oggetti `Messagge` dal `Server` a tutti i `Client`, ma solo il `Client` identificato dall'indirizzo presente nel messaggio potrà conservare e gestire tale messaggio per poterlo mostrare a terminale. Ogni `Message` possiede un'intestazione `Header` ed un corpo `Body`.

Un `Header` ha due campi: un titolo del messaggio e un array di indirizzi dei `Client` selezionati per questo messaggio.

Un `Body`, il secondo campo di un `Message`, è una collezione di `String` per un unico testo che assume diversi colori se stampato a terminale.

In `Main` si simuli con ritardi *random* l'invio dei messaggi ai `Client` da parte del `Server` in modo che, raggiunti i
`Client` questi stampino il messaggio a terminale, opportunamente colorato e con il titolo di `Header`, solo se tale messaggio (vedi indirizzo) è a loro destinato.

# chatroom (D)

In questo tipo di cosengna simuleremo su di un terminale unico una *chatrrom*: una stanza in cui scrivono più `User` utenti che dovranno essere registrati. Ecco il protocollo:
- `login <user>` permette di accedere al servizio come utente registrato `<user>` ed inviare messaggi,
- `logout <user>` riporta il servizio in attesa di un nuovo ingresso o di una registrazione,
- `signin <user>` permette di registrare un utente (nessun utente deve essere connesso).
Una volta loggato l'utente potrà messaggiare dopo che gli saranno esposti i messaggi presenti nella *chatroom*. I messaggi per arrivare al `Server` impiegheranno un tempo random fra `0.5` e `1.5` secondi. SI lascia massima libertà per il resto. Per leggere da terminale si usi l'oggetto `Console`.
**Hint**: i messaggi siano inviati al server mediante oggetto `Message` opportunamente progettato e poi inviati a tutti gli utenti. 

## pattern

Come scritto useremo lo schema di progetto presentato in [1].

## materiali

[1] Il progetto IDEA `events` [qui](https://gitlab.com/divino.marchese/zuccante_src/-/tree/master/java/events?ref_type=heads)

## esempio d'uso della `Console` e metodo `sleep`

Forniamo come ulteriore materiale il codice seguente.
```java
import java.io.Console;

public class Test {

    public static void main(String[] args) throws Exception {
        Console console = System.console();
        if (console == null) {
            System.out.println("Unable to fetch console");
            return;
        }
        String line = console.readLine();
        console.printf("I saw this line: %s", line);
    }
}
```

```java
public class Sleep {

    public static void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public static void main(String[] args) {
        System.out.println("messaggio 01");
        sleep(2000);
        System.out.println("messaggio 02");
    }
}
```

## valutazione consegne

Il progetto IDEA Gradle possiede in `README.md` la documetazione,
per ogni sessione di laboratorio vi sarà un *commit* di fine sessione in cui si presenta il lavoro provvisorio, altri *commit* fatti non durante le sessioni di laboratorio sono ammessi.

Contribuiscono alla valutazione: il rispetto delle consegne, la pulizia del codice, la documentazione e le scelte operate. Le valutazioni `ottimo` ed `eccellente` sono riservate a coloro che daranno un consistente 
apporto personale e creativo con attenzione all'usabilità dell'applicazione, e che si dedicheranno alla versione **D**. 
