import java.util.Random;

public class ZMatrix {

    private static Random rnd = new Random();

    private int[][] matrix;
    private int n; // number of columns
    private int m; // number of rows

    public ZMatrix(int m, int n) {
        // create a m x n matrix with random int in 0..10 
    }

    public int[] getSizes() {
        // return [m, n]
    }

    public int[] getRow(int i) {
        // get i row
    }

    public int[] getColumn(int j) {
        // get j column
    }

    public int[][] getColumns() {
        // obtain columns array
    }

    public int getElement(int i, int j) {
        return matrix[i][j];
    }

    public void setElement(int i, int j, int d) {
        matrix[i][j] = d; 
    }

    public boolean isSymmetric() {

    }

    public static ZMatrix transpose(ZMatrix m) {

    }

    @Override 
    public String toString() {

    }
    
}
