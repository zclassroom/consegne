public class HM241111 {
    
    private int[] array;
    private int size;

    public Lab241030(int[] array) {
        this.array = array;
        size = array.length;
    }

    private void swap(int i, int j){
        /* 
         * scambia fra loro gli elementi 
         * in posizione i e j di array
         */
    }

    public int rMin() {
        /* 
         * ritornal minimo di array
         * usa rMin(0)
         */
    }

    private int rMin(int i) {
        /* 
         * ricorsiva, ritorna il minimo 
         * a partire dalla posizione i
         */
    }

    public void rRoll() {
        /*
         * gira specularmente array
         * usa rRoll(0, size -1)
         */
    }

    private void rRoll(int i, int j) {
        /*
         * ricorsiva, gira specularmente array 
         * da i a j = size - 1 - i
         */
    }

    public int rZeros() {
        /* 
         * conta gli zeri di array
         * usa rZeros(0)
         */
    }

    private int rZeros(int i) {
        /* 
         * ricorsiva, conta gli zeri di array
         * a partire dalla posizione i
         */
    }

    public static void main(String[] args) {
        // testing
    }
}
