# prenotazioni e attività

- commit: *dump* `sql`
- deadline 06/02/2024

Le tabelle sono
```
activities(id, name, date_time_start, date_time_end, n, n_max)
```
`n` il numero di prenotazioni e `n_max` il numero massimo di prenotazioni, e
```
reservations(id, subscriber, id_activity)
```
Creare il DB, popolarlo. Creare i due *trigger* che incrementano e diminuiscono `n` ogni volta che viene creata e cancellata una prenotazione.
Alcune *query* di test. Test dei *trigger*.