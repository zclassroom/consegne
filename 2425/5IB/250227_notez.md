# notez

- deadline 13/02/2025

Una app Flutter di nome `notez`con le seguenti caratteristiche:
- usa un db interno di tipo `ObjectBox` per mantenere in modo permanete le note,
- presenta le note su delle *card* opportunamente disposte,
- ogni nota ha un titolo e del testo,
- le note possono avere dei tag, i tag permettono di organizzare e visualizzare le note,
- le note possono essere create, modificate e cancellate,
- la connessione al db va gestita tramite un `Provider`

## valutazione

Il progetto, nell'apposito repo per Informatica, si chiama `notez` e contiene in `README.md` la documentazione. Rispetto delle specifiche, pulizia ed ordine nel codice, ed usabilità sono elementi di valutazione. Si terrà conto di ogni apporto personale e creativo, esso è necessario per le valutazioni eccellenti.

