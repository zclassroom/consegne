# tris concorrente

Si realizzi il Tris in modo concorrente: i due giocatori, due `Thread` `Player`, giocano su di uno `Schema` che risulta essere una risorsa non condivisibile. 
- Ogni giocatore pensa alla sua mossa in un tempo che va dai `0.5s` a `1.0s` e gioca in modo casuale.
- Nella classe `Main` viene tentata una partita.

## alcuni riferimenti

[1] API Java su ReentrantLock.  
[2] "Binary Semaphore vs Reentrant Lock" su Baeldung  [qui](https://www.baeldung.com/java-binary-semaphore-vs-reentrant-lock).  

## dead line

Entro le 23:59 del 16/01/2025

## valutazione

Contribuiscono alla valutazione: la pulizia del codice, la documentazione e le scelte operate. La cartella di progetto è `tris_concorrente`, il(*repository* è `TPSIT` (o nome comunicato ai docenti), la documentazione va nel file `README.md`. Le valutazioni `ottimo` ed `eccellente` sono riservate a coloro che daranno un consistente 
apporto personale e creativo con attenzione all'usabilità dell'applicazione; verrà valutata anche la possibilità di un approccio "intelligente" alla mossa. Si usi in IDEA un progetto Gradle.
