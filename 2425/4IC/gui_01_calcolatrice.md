# calcolatrice

Proponiamo la seguente esercitazione da realizzare con `swing` possibilmente usando i temi.

Trattasi di una calcolatrice non scientifica con le 4 operazioni e registro di accumulo. La presenza del registro di accumulo come 
è noto permette di utilizzare il risultato di una operazione come primo operando dei una successiva operazione.

A tal fine verranno realizzate le classi:
- `ZCalc` che estende `JFrame` e
- `State` per realizzare la macchina a stati finiti della calcolatrice: riceve in ingresso gli input della calcolatrice, ovvero
numeri e simboli, e ritorna il valore del reghistro di accumulo cioè il risultato al momento in cui usiamo `=`.
Potrebbe risultare interessante l'uso di uno *stack* interno come memoria.

## alcuni riferimenti

[1] "Oracle Swing tutorials" [qui](https://docs.oracle.com/javase/tutorial/uiswing/).  
[2] "Java Swing component" da Oracle [qui](https://web.mit.edu/6.005/www/sp14/psets/ps4/java-6-tutorial/components.html).  
[3] Esempi del docente [qui](https://gitlab.com/divino.marchese/zuccante_src/-/tree/master/java/swing?ref_type=heads).  
[4] FlatLaf [qui](https://www.formdev.com/flatlaf/themes/).  

## dead line

Entro le 23:59 del 30/11/2024

## valutazione

Contribuiscono alla valutazione: la pulizia del codice, la documentazione e le scelte operate. La cartella di progetto è `zcalc`, il progetto (repository) è `TPSIT` 
(o nome comunicato al docente), la documentazione va nel file `README.md`. Le valutazioni `ottimo` ed `eccellente` sono riservate a coloro che daranno un consistente 
apporto personale e creativo con attenzione all'usabilità dell'applicazione, inoltre useranno il tema `FlatLaf` ed uno *stack* come mmeoria interna.
