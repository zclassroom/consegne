# rendezvous matriciale

Si realizzi il prodotto di due matrici `nXn`. Ad ogni elemento di tale matrice dedichiamo un *thread*
viene dedicato al prodotto riga per colonna. Una volta che gli `n²` *thread* hanno terminato il loro lavoro un ultimo *thread* calcola la somma degli elementi della matrice prodotto.


## dead line

Entro le 23:59 del 20/02/2025

## valutazione

Contribuiscono alla valutazione: la pulizia del codice, la documentazione e le scelte operate. La cartella di progetto è `rendezvous_matriciale`, il *repository* è `TPSIT` (o nome comunicato ai docenti), la documentazione va nel file `README.md`. Le valutazioni `ottimo` ed `eccellente` sono riservate a coloro che daranno un consistente 
apporto personale e creativo con attenzione all'usabilità dell'applicazione con test di velocità. Si usi in IDEA un progetto Gradle.
